# README

## Ruby version

2.4.1

## Setup
* Install [rvm](https://rvm.io/)
* Install ruby 2.4.1 - `rvm install 2.4.1`

## Steps to run the project
* cd reward_system
* rails db:create
* rake db:migrate
* bundle install

## How to Run the App
* rails s
* visit 'http://localhost:3000/' on browser
* you will see Form to upload events
* click on 'choose file' and select 'event.json' file
* 'event.json' file is present in 'reward_system' project
* click on save
* the app will process all the events based on timestamps and assign appropriate points.
* the result appears in JSON format.

## Design
* The app uses 2 models 'Customer' and 'Recommendation'
* Recommendation is created when a customer recommends another user
* Customer is created in the system when a any recommendation is accepted
* Customer model stores points in decimal format
* points are assigned to customer every time a event for acceptance is received  
