class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :name
      t.integer :parent_id
      t.decimal :points

      t.timestamps
    end
    add_index :customers, :parent_id
  end
end
