class CreateRecommendations < ActiveRecord::Migration[5.1]
  def change
    create_table :recommendations do |t|
      t.string :name
      t.integer :customer_id
      t.boolean :accepted

      t.timestamps
    end
    add_index :recommendations, :customer_id
    add_index :recommendations, :accepted
    add_index :recommendations, :name
  end
end
