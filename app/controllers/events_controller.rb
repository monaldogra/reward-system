class EventsController < ApplicationController
  def new
  end

  def create
    json_file = params[:event_file].read
    json_data = JSON.parse(json_file)
    res = Event.new.process_events(json_data)
    Customer.destroy_all
    Recommendation.destroy_all

    render json: { points: res }, status: :ok
  end
end
