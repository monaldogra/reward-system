class Customer < ApplicationRecord
  belongs_to :parent, class_name: 'Customer', foreign_key: 'parent_id', optional: true

  has_many :recommended_customers, class_name: 'Recommendation', foreign_key: 'customer_id'
end
