class Event
  def process_events(data)
    data = data.sort_by { |e| e['datetime'].to_datetime }

    data.each do |e|
      if e['event'].include? 'recommends'
        arr = e['event'].split(" ")
        cust1 = arr[0]
        cust2 = arr[2]
        customer = Customer.find_by(name: cust1)
        unless customer
          customer = Customer.new(name: cust1)
          customer.save
        end
        customer.recommended_customers.create(name: cust2, accepted: false)

      elsif e['event'].include? 'accepts'
        arr = e['event'].split(" ")
        cus = arr[0]
        next if Recommendation.exists?(name: cus, accepted: true)

        rc = Recommendation.find_by(name: cus, accepted: false)
        next unless rc

        rc.accepted = true
        rc.save
        c = Customer.create(name: cus, parent_id: rc.recommended_by.id)

        assign_points(c)
      else
        next
      end
    end

    show_results
  end

  def assign_points(customer)
    d = 1
    parent = customer.parent

    while parent
      parent.points = parent.points.to_f + 1/d.to_f
      parent.save
      d *= 2
      parent = parent.parent
    end
  end

  def show_results
    res = {}
    Customer.where.not(points: nil).pluck(:name, :points).each do |i|
      points =  i[1] = i[1] % 1 == 0 ? i[1].to_i : i[1].to_f
      res[i[0]] = points
    end
    res
  end
end
