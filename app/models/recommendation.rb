class Recommendation < ApplicationRecord
  belongs_to :recommended_by, class_name: 'Customer', foreign_key: 'customer_id'
end
